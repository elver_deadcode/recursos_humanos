@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Destino Ubicacion</h3>
            <a  href="#myModal"  data-toggle="modal" class=" btn  btn-success">
                <i class="fa fa-plus"></i> Nuevo registro </a>
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Registrar Destino Ubicacion</h4>

                        </div>
                        <form action="{{url('admin/lugares/store')}}" method="post"  class="form-horizontal" id="form" >
                            {!! csrf_field() !!}
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="" class="col-md-2">Nombre</label>
                                    <div class="col-md-6">
                                        <input type="text" name="nombre" id="nombre" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-2">Codigo</label>
                                    <div class="col-md-2">
                                        <input type="text" name="codigo" id="codigo" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-success" >Guardar</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="panel-body">
            <table class="dataTable table" id="table">
                <thead>
                <tr>
                    <th>Nro</th>
                    <th>Codigo</th>
                    <th>Nombres</th>
                    <th>Acciones</th>
                </tr>
                </thead>

                <tbody>
                @php  $i=1;@endphp
                @foreach($lugares as  $row)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$row->codigo}}</td>
                        <td>{{$row->nombre}}</td>
                        <td>
                            <a href="#myModal{{$row->id}}"  data-toggle="modal" class="btn btn-warning"><i class="fa fa-pencil"></i> Editar</a>
                            <div id="myModal{{$row->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Editar Destino Ubicacion</h4>
                                        </div>
                                        <form action="{{url('admin/lugares/update')}}" method="post"  class="form-horizontal" id="form" >
                                            {!! csrf_field() !!}
                                            <input type="hidden"  name="id" value="{{$row->id}}">
                                            <div class="modal-body">

                                                <div class="form-group">
                                                    <label for="" class="col-md-2">Nombre</label>
                                                    <div class="col-md-6">
                                                        <input type="text" value="{{$row->nombre}}" name="nombre" id="nombre" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-2">Codigo</label>
                                                    <div class="col-md-2">
                                                        <input type="text" name="codigo" value="{{$row->codigo}}" id="tipo" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-success" >Guardar</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>

                            <a href="{{url('admin/lugares/destroy')}}/{{$row->id}}" class="btn btn-danger"><i class="fa fa-"></i> Borrar</a>
                        </td>

                    </tr>
                    @php  $i++;@endphp
                @endforeach

                </tbody>

            </table>
        </div>


    </div>
@stop