@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Secciones</h3>
            <a  href="#myModal"  data-toggle="modal" class=" btn  btn-success"><i class="fa fa-plus"></i> Nuevo registro </a>


            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Registrar Seccion</h4>
                        </div>
                        <form action="{{url('admin/secciones/store')}}" method="post"  class="form-horizontal" id="form" >
                             {!! csrf_field() !!}
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="" class="col-md-2">Nombre</label>
                                <div class="col-md-6">
                                <input type="text" name="nombre" id="nombre" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-2">Tipo</label>
                                <div class="col-md-2">
                                    <input type="text" name="tipo" id="tipo" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-success" >Guardar</button>
                        </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="panel-body">
            <table class="dataTable table" id="table">
                <thead>
                <tr>
                    <th>Nro</th>
                    <th>tipo</th>
                    <th>Nombres</th>
                    <th>Acciones</th>
                </tr>
                </thead>

                <tbody>
                @php  $i=1;@endphp
                @foreach($secciones as  $row)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$row->tipo}}</td>
                        <td>{{$row->nombre}}</td>
                        <td>
                            <a href="#myModal{{$row->id}}"  data-toggle="modal" class="btn btn-warning"><i class="fa fa-pencil"></i> Editar</a>
                            <div id="myModal{{$row->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Registrar Seccion</h4>
                                        </div>
                                        <form action="{{url('admin/secciones/update')}}" method="post"  class="form-horizontal" id="form" >
                                            {!! csrf_field() !!}
                                            <input type="hidden"  name="id" value="{{$row->id}}">
                                            <div class="modal-body">

                                                <div class="form-group">
                                                    <label for="" class="col-md-2">Nombre</label>
                                                    <div class="col-md-6">
                                                        <input type="text" value="{{$row->nombre}}" name="nombre" id="nombre" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-2">Tipo</label>
                                                    <div class="col-md-2">
                                                        <input type="text" name="tipo" value="{{$row->tipo}}" id="tipo" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-success" >Guardar</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>

                            <a href="{{url('admin/secciones/destroy')}}/{{$row->id}}" class="btn btn-danger"><i class="fa fa-"></i> Borrar</a>
                        </td>

                    </tr>
                    @php  $i++;@endphp
                    @endforeach

                </tbody>

            </table>
        </div>


    </div>
@stop