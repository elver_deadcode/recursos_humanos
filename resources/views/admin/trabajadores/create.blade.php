@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Trabajadores / Registro trabajador</h3>
        </div>
        <div class="panel-body">
            <div id="wizard" class="form_wizard wizard_horizontal">
                <ul class="wizard_steps">
                    <li>
                        <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                            Paso 1<br />
                                            <small> Datos Personales</small>
                                        </span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                            Paso 2<br />
                                            <small>Datos Institucionales</small>
                                        </span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                            Paso 3<br />
                                            <small>Contratos</small>
                                        </span>
                        </a>
                    </li>


                </ul>
                <form class=" form form-horizontal form-label-left"  enctype="multipart/form-data" id="formFichas" method="POST" action="{{url('admin/trabajadores/store')}}">
                    {!! csrf_field() !!}
                    <div id="step-1">


                        <div class="form-group">
                            <div class="form-group" style="margin-left:12px">
                                <label class="control-label col-md-3 ">Nombres<span class="required">*</span>
                                </label>
                                <div class="col-md-3 " style="margin-left: 12px">
                                    <input type="text" name="nombres" placeholder="nombres" id="nombres" class="form-control">
                                </div>
                                <div class="col-md-3 ">
                                    <input type="text" name="apellidos" placeholder="apellidos" id="apellidos" class="form-control">
                                </div>
                            </div>
                            <label class="control-label col-md-3 " for="first-name">Nacionalidad<span class="required">*</span>
                            </label>
                            <div class="col-md-2 ">
                                <div class="col-md-12" >
                                    <input type="text" value="BOLIVIANO" name="nacionalidad" placeholder="nacionalidad" id="nacionalidad" class="form-control">
                                </div>


                            </div>


                            <label for="" class="">Tipo de documento</label>

                            <div class="col-md-2 ">

                                <select name="tipo_documento" id="tipo_documento" class=" form-control">
                                    <option value="carnet">Carnet</option>
                                    <option value="pasaporte">Pasaporte</option>
                                    <option value="licencia conducir">Licencia de Conducir</option>
                                    <option value="dni">DNI</option>
                                    <option value="otro">otro</option>

                                </select>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Num documento<span class="required">*</span></label>
                            <div class="col-md-2 ">
                                <input type="text" name="num_documento" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Sexo<span class="required">*</span></label>
                            <div class="col-md-2 ">
                                <select name="sexo" id="sexo"class="form-control">
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Estado Civil<span class="required">*</span></label>
                            <div class="col-md-2 ">
                                <select name="estado_civil" id="estado_civil"class="form-control">
                                    <option value="soltero">Soltero/a</option>
                                    <option value="casado">Casado/a</option>
                                    <option value="viudo">Viudo/a</option>
                                    <option value="divorsiado">Divorsiado/a</option>
                                    <option value="separado">Separado/a</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Profesion/Ocupacion
                            </label>
                            <div class="col-md-6 ">
                                <input type="text" name="profesion" placeholder="profesion/ocupacion" id="profesion" class="form-control">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Direccion
                            </label>
                            <div class="col-md-6 ">
                                <input type="text" name="direccion" placeholder="direccion" id="direccion" class="form-control">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">N° Seguro Social
                            </label>
                            <div class="col-md-3 ">
                                <input type="text" name="nro_social" placeholder="numero seguro social" id="nro_social" class="form-control">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 ">Telefono Fijo
                            </label>
                            <div class="col-md-3 ">
                                <input type="text" name="telefono_fijo" placeholder="telefono fijo" id="telefono_fijo" class="form-control">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Telefono Celular
                            </label>
                            <div class="col-md-3 ">
                                <input type="text" name="telefono_celular" placeholder="telefono celular" id="telefono_celular" class="form-control">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Telefono Emergencia
                            </label>
                            <div class="col-md-3 ">
                                <input type="text" name="telefono_emergencia" placeholder="telefono emergencia" id="telefono_emergencia" class="form-control">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Telefono Coorporativo
                            </label>
                            <div class="col-md-3 ">
                                <input type="text" name="telefono_coorporativo" placeholder="telefono coorporativo" id="telefono_coorporativo" class="form-control">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Nro Licencia
                            </label>
                            <div class="col-md-3 ">
                                <input type="text" name="nro_licencia" placeholder="licencia de conducir" id="nro_licencia" class="form-control">
                            </div>
                            <label for="" class="control-label pull-left">Categoria</label>
                            <div class="col-md-2 col-sm-1 col-xs-12">

                                <select name="cat_licencia" id="cat_licencia" class="form-control">
                                    <option value="a">A</option>
                                    <option value="b">B</option>
                                    <option value="C">C</option>
                                </select>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Fecha Fenecimiento
                            </label>
                            <div class="col-md-2 ">
                                <input  id="birthday" type="text"  value="{{date('d/m/Y')}}"  readonly name="fecha_fenecimiento"  class="date-picker form-control">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Correo electronico
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="text" name="email_personal"   placeholder="correo personal" id="email_personal" class="form-control">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Correo Institucional
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="text" name="email_profesional"   placeholder="correo institucional" id="email_profesional" class="form-control">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 ">Num de Cuenta</label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="text" name="num_cuenta"   placeholder="numero de cuenta" id="num_cuenta" class="form-control">
                            </div>

                        </div>
                        <button type="button" id="btn_familia" class=" col-md-6  col-md-offset-3 btn btn-success"><i class="fa fa-users"></i> +Registrar Familiares</button>

                        <div id="pnl_familia" class=" panel panel-default" style="display: none">

                            <div class="panel-body" >
                                <table>
                              <tr class="fila">
                                <div class="form-group">
                                    <td>

                                    <div class="col-md-12">
                                        <label class="control-label ">Nombres</label>
                                        <input type="text" name="nombres_familiar[]"   placeholder="Nombre completo"  class="nombres_familiar form-control">
                                    </div>

                                    </td>
                                    <td>
                                    <div class="col-md-12">
                                        <label class="control-label ">Parentesco</label>
                                        <input type="text" name="parentesco_familiar[]"   placeholder="Parentesco"  class="parentesco_familiar form-control">
                                    </div>
                                    </td>
                                    <td>
                                    <div class="col-md-12">
                                        <label class="control-label ">Fecha Nac</label>
                                        <input type="text" name="fechanac_familiar[]"   readonly value="{{date('d/m/Y')}}" class="date-picker form-control">
                                    </div>
                                    </td>
                                    <td>
                                    <div class="col-md-12">
                                        <label class="control-label ">Carnet</label>
                                        <input type="text" name="carnet_familiar[]"    class="carnet_familiar form-control">
                                    </div>
                                    </td>
                                    <td >

                                    <div class=" opciones col-md-12">

                                    <button type="button" onclick="clonar(this)" class="  btn btn-success btn-sm" style="margin-top:30px;"> <i class="fa fa-plus"></i></button>
                                        </div>
                                    </td>
                                </div>
                              </tr>
                                </table>
                            </div>


                        </div>


                    </div>

                    <div id="step-2">
                        <h2 class="StepTitle">Datos Institucionales</h2>

                        <div class="form-group">
                            <label class="control-label col-md-3 ">Item
                            </label>
                            <div class="col-md-2 ">
                                <input type="text" name="item" placeholder="item " id="item" class="form-control">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 ">Cargo
                            </label>
                            <div class="col-md-2 ">
                                <input type="text" name="cargo" placeholder="cargo" id="cargo" class="form-control">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 ">Haber Basico (Bs)
                            </label>
                            <div class="col-md-2 ">
                                <input type="text" name="sueldo" placeholder="sueldo " id="sueldo" class="form-control">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 ">Destino Ubicacion
                            </label>
                            <div class="col-md-4 ">
                                <select name="lugar_id" id="lugar_id" class="form-control">
                                    <option value="">Seleccione Destino</option>
                                    @foreach($lugares as $row)
                                        <option value="{{$row->id}}">{{$row->nombre}}</option>
                                        @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 ">Sector trabajo
                            </label>
                            <div class="col-md-4 ">
                                <select name="seccion_id" id="sector_id" class="form-control">
                                    <option value="">Seleccione Sector</option>
                                    @foreach($secciones as $row)
                                        <option value="{{$row->id}}">{{$row->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>


                        <div class="form-group">
                            <label style="margin-right: 9px" class="control-label col-md-3 ">Antiguedad Extra Inst
                            </label>
                            <label for="email" class="sr-only">Email</label>
                            <div class="input-group col-sm-5">
                                  <span class="input-group-addon">
                                    Años
                                  </span>
                                <input type="number" id="anios_ant" name="anios_ant" class="form-control" placeholder="anos" />
                                <span class="input-group-addon">
                                    Mes
                                 </span>
                                <input type="number" id="mes_ant" name="mes_ant" class="form-control" placeholder="meses" />
                                <span class="input-group-addon">
                                    Dias
                                 </span>
                                <input type="number" id="dias_ant" name="dias_ant" class="form-control" placeholder="dias" />
                            </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 ">Fecha Ingreso
                            </label>
                            <div class="col-md-2 ">
                                <input type="text" name="fecha_ingreso" placeholder=""  value="{{date('d/m/Y')}}"  readonly id="fecha_ingreso" class=" date-picker form-control">
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 ">Tipo Funcionario
                            </label>
                            <div class="col-md-2 ">
                                <select name="tipo_funcionario" id="tipo_funcionario" class="form-control">

                                    <option value="A">PLANTA</option>
                                    <option value="T">CONTRATO</option>
                                    <option value="C">CONSULTOR</option>
                                    {{--<option value="E">EVENTUAL</option>--}}
                                    {{--<option value="D">CONVENIO</option>--}}


                                </select>
                            </div>

                        </div>


                    </div>
                    </div>
                    <div id="step-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <button type="button" class="btn btn-success" id="agregar_cto"><i class="fa fa-file-text"></i> +Agregar contrato</button>
                            </div>
                        </div>
                        <div class="panel-body">



                        </div>
                        <div style="display: none;"  id="form-contrato" class="form-contrato">


                            <div class="form-group">

                                <label for="" class="col-md-2 control-label">COD CONTRATO</label>

                                <div class="col-md-4">
                                    <input type="text"  id="cod_contrato" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">

                                <label for="" class="col-md-2 control-label">TIPO CONTRATO</label>

                                <div class="col-md-4">
                                    <select  id="tipo_contrato" class="form-control">
                                        <option value="PF">PLAZO FIJO</option>
                                        <option value="CM">CONCLUSION MODULO</option>
                                        <option value="CP">CONCLUSION PROYECTO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">

                                <label for="" class="col-md-2 control-label">PROYECTO A CARGO</label>

                                <div class="col-md-4">
                                    <select  class="form-control"  id="proyecto">
                                        @foreach($proyectos as $p)
                                            <option value="{{$p->id}}">{{$p->descripcion}}</option>
                                            @endforeach()
                                    </select>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="" class="col-md-2 control-label">FECHA INICIO</label>
                                <div class="col-md-2">
                                    <input type="text" readonly  id="fecha_ini_cto" class="date-picker form-control">
                                </div>
                                <label for="" class=" col-md-2 control-label">FECHA FINALIZACION</label>
                                <div class="col-md-2">
                                    <input type="text"  readonly id="fecha_fin_cto" class="date-picker form-control">
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="" class="col-md-2 control-label">DIRECTOR FIRMA CTO</label>

                                <div class="col-md-4">
                                    <input type="text"  id="firma_cto" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">

                                <label for="" class="col-md-2 control-label">ASESOR LEGAL ELAB CTO.</label>

                                <div class="col-md-4">
                                    <input type="text" id="asesor_cto" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">

                                <label for="" class="col-md-2 control-label">OBSERVACIONES</label>

                                <div class="col-md-4">
                                    <textarea  id="observaciones_cto" class="form-control" cols="30" rows="3"></textarea>
                                </div>
                            </div>
                            {{--<div class="form-group">--}}

                                {{--<label for="" class="col-md-2 control-label">ARCHIVO ASOCIADO</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input type="file" class="form-control">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group ">
                                <div class="col-md-4 col-md-offset-3">
                                    <input type="hidden" id="sw" value="g">
                                <button class="btn btn-success" type="button" id="btn_agregar">GUARDAR</button>
                                <button class="btn btn-default" type="button" id="btn_cancelar">CANCELAR</button>
                                </div>
                            </div>

                        </div>




                        <table id="table-contrato" class="table">
                            <thead>
                            <tr >
                                <th>COD CONTRATO</th>
                                <th>FECHA INICIO</th>
                                <th>FECHA FINALIZACION</th>
                                <th>TIPO CONTRATO</th>
                                <th>ADJUNTAR ARCHIVO</th>
                                <th>ACCIONES</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </form>

            </div>


         </div>
    </div>
    @if (Session::has('notifier.notice'))
        <script>
            new PNotify({!! Session::get('notifier.notice') !!});
        </script>
    @endif

@stop

@section('myscript')

    <script type="text/javascript" src="{{asset('/js/wizard/jquery.smartWizard.js')}}"></script>

    <script type="text/javascript">
        count_cult=0;

        var fila_id=0;
        var count_filas=0;
        function clonar(boton){

            var $tr= $(boton).closest('.fila');
            $nombres=$tr.find('.nombres_familiar').val();
            $parentesco=$tr.find('.parentesco_familiar').val();
            if($nombres=='' || $parentesco==''){
                alert('Debe ingesar nombre de familiar');
                $tr.find('.nombres_familiar').focus();
            }
            else {
//
                $(boton).fadeOut(10);
                var $clone = $tr.clone();

                $tr.after($clone);
                $clone.find('.nombres_familiar').focus();
                $clone.find('.parentesco_familiar').val('');
                $clone.find('.carnet_familiar').val('');
                $clone.find('.nombres_familiar').val('');

                $tr.find('.opciones').append('<button type="button" class="rmv btn btn-sm btn-danger" ><i class="fa fa-times"</i></button>');
            }

        }
            function editar_fila(btn){

                fila_id=$(btn).closest('tr').prop('id');

                $('#sw').val('e');
                $('#cod_contrato').val($(btn).closest('tr').find('#f_cod_contrato').val());
                $('#proyecto').val($(btn).closest('tr').find('#f_proyecto').val());
                $('#firma_cto').val($(btn).closest('tr').find('#f_firma_cto').val());
                $('#asesor_cto').val($(btn).closest('tr').find('#f_asesor_cto').val());
                $('#observaciones_cto').val($(btn).closest('tr').find('#f_observaciones_cto').val());
                $('#fecha_ini_cto').val($(btn).closest('tr').find('#f_fecha_ini_cto').val());
                $('#fecha_fin_cto').val($(btn).closest('tr').find('#f_fecha_fin_cto').val());
                $('#tipo_contrato').val($(btn).closest('tr').find('#f_tipo_contrato').val());
                $('#table-contrato').hide();
                $('#form-contrato').fadeIn(150);
                $('#agregar_cto').hide();
            }

            function limpiar_form_contratos(){
                $('#cod_contrato').val('');
                $('#proyecto').val(1);
                $('#firma_cto').val('');
                $('#asesor_cto').val('');
                $('#observaciones_cto').val('');
                $('#fecha_ini_cto').val('');
                $('#fecha_fin_cto').val('');
                $('#tipo_contrato').val(1);

            }

            function eliminar_fila(btn){
                $(btn).closest('tr').remove();
                count_filas--;

            }

        $(document).ready(function() {


            $('#btn_familia').on('click',function(_evt){

                $(this).hide();
                $('#pnl_familia').fadeIn(150);
            });



            $('#btn_agregar').click(function(){

                 var sw=$('#sw').val();


                if(sw=='g') {
                    var fila = '';
                    fila += '<tr id="' + count_filas + '">' +
                            '<td>' +
                            '<input type="text" id="f_cod_contrato" readonly class="form-control f_cod_contrato" value="' + $('#cod_contrato').val() + '" name="cod_contrato[]" class="form-control">' +
                            '<input type="hidden" id="f_proyecto" class="form-control f_proyecto" value="' + $('#proyecto').val() + '" name="proyecto[]" class="form-control">' +
                            '<input type="hidden" id="f_firma_cto" class="form-control f_firma_cto" value="' + $('#firma_cto').val() + '" name="firma_cto[]" class="form-control">' +
                            '<input type="hidden" id="f_asesor_cto" class="form-control f_asesor_cto" value="' + $('#asesor_cto').val() + '" name="asesor_cto[]" class="form-control">' +
                            '<input type="hidden" id="f_observaciones_cto" class="form-control f_observaciones_cto" value="' + $('#observaciones_cto').val() + '" name="observaciones_cto[]" class="form-control">' +
                            '</td>' +
                            '<td><input type="text" readonly id="f_fecha_ini_cto" class="form-control f_fecha_ini_cto" value="' + $('#fecha_ini_cto').val() + '" name="fecha_ini_cto[]" class="form-control"></td>' +
                            '<td><input type="text" readonly id="f_fecha_fin_cto" class="form-control f_fecha_fin_cto" value="' + $('#fecha_fin_cto').val() + '" name="fecha_fin_cto[]" class="form-control"></td>' +
                            '<td><input type="text" readonly id="f_tipo_contrato" class="form-control f_tipo_contrato" value="' + $('#tipo_contrato').val() + '" name="tipo_contrato[]" class="form-control"></td>' +
                            '<td><input type="file"  name="archivo[]" class="form-control"></td>' +
                            '<td>' +
                            '<a class="edit btn btn-warning btn-xs" onclick="editar_fila(this)" ><i class="fa fa-pencil"></i></a>' +
                            '<a class="delete btn btn-danger btn-xs" onclick="eliminar_fila(this)" ><i class="fa fa-close"></i></a>' +
                            '</td>' +
                            '</tr>';

                    $('#table-contrato tbody').append(fila);
                    $('#table-contrato').fadeIn(150);
                    $('#form-contrato').hide(150);
                    $('#agregar_cto').show();
                    limpiar_form_contratos();
                    count_filas++;
                }
                else{
                    var fedit=$('#table-contrato tbody').find($('#'+fila_id));

                    $(fedit).find($('.f_cod_contrato')).val($('#cod_contrato').val());
                    $(fedit).find($('.f_proyecto')).val($('#proyecto').val());
                    $(fedit).find($('.f_firma_cto')).val($('#firma_cto').val());
                    $(fedit).find($('.f_asesor_cto')).val($('#asesor_cto').val());
                    $(fedit).find($('.f_observaciones_cto')).val($('#observaciones_cto').val() );
                    $(fedit).find($('.f_fecha_ini_cto')).val($('#fecha_ini_cto').val()  );
                    $(fedit).find($('.f_fecha_fin_cto')).val($('#fecha_fin_cto').val());
                    $(fedit).find($('.f_tipo_contrato')).val($('#tipo_contrato').val());
                    $('#table-contrato').fadeIn(150);
                    $('#form-contrato').hide(150);
                    $('#agregar_cto').show();
                    limpiar_form_contratos();
                    $('#sw').val('g');

                }


            });



            $('#btn_cancelar').click(function(){
                $('#table-contrato').fadeIn(150);
                $('#form-contrato').hide(150);
                $('#agregar_cto').show();
                limpiar_form_contratos();


            });

            $('#agregar_cto').click(function(){
         $('#table-contrato').hide();
         $('#form-contrato').fadeIn(150);
         $(this).hide();
            });

            $('body').on('click', '.rmv',function(){
                $tr=$(this).closest('tr');

                $tr.remove();

            });

           // Smart Wizard
            $('#wizard').smartWizard({
                transitionEffect: 'slide',
                enableFinishButton: false,
                onLeaveStep:leaveAStepCallback,
                onFinish:onFinishCallback,
            });
            function leaveAStepCallback(obj){
                var step_num= obj.attr('rel'); // get the current step number

                return validateSteps(step_num); // return false to stay on step and true to continue navigation
            }

            function onFinishCallback(){
                if(validateAllSteps()){
                    $('form').submit();
                }
            }
            function validateSteps(stepnumber){
                var isStepValid = true;

//                if(stepnumber == 1){
//                    var route='/fichas/validarMunicipio/'+$('#municipios_id').val()+"/"+$("#gestion").val();
//                    $.ajax({
//                        url:route,
//                        method:'GET',
//                        async:false,
//                        dataType:'json',
//                        success:function(response){
//
//                            if(response.status==200){
//                                alert('Esta ficha en esta getion  ya existe');
//                                isStepValid=false;
//                            }
//                        }
//
//                    });
//
//                }
//                if(stepnumber==2){
//                    if(count_cult<=0){
//                        alert('Debes ingresar un cultivo');
//                        isStepValid=false;
//                    }
//                }
//                if(stepnumber==3){
//                    if(count_ganado<=0){
//                        alert('Debes ingresar una especie');
//                        isStepValid=false;
//                    }
//                }
//                if(stepnumber==4){
//                    if(count_maquinaria<=0){
//                        alert('Debes ingresar un maquinaria');
//                        isStepValid=false;
//                    }
//                }

                return isStepValid;

            }
            function validateAllSteps(){
                var isStepValid = true;
                // all step validation logic
                return isStepValid;
            }

        });

    </script>

    @stop