@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Vacaciones</h3>
        </div>
        <div class="panel-body">
            <div class="col-md-6">
            <h4>NOMBRE:<strong>{{$trabajador->nombre_completo}}</strong></h4>
            <h4>CARGO :<strong>{{$trabajador->cargo}}</strong></h4>
            </div>
            <div class="col-md-6">
                <h4>F. INGRESO:<strong>{{$trabajador->fecha_ingreso->format('d/m/Y')}}</strong></h4>
                <h4>ANTIGUEDAD :<strong>{{$trabajador->antiguedad()}} AÑOS</strong></h4>
                </div>
            @if(count($datos))
                <table class="table dataTable">
                    <thead>
                    <tr>
                        <th>GESTION</th>
                        <th>ANTIGUEDAD <br>AÑOS</th>
                        <th>DIAS DE VACACIONES</th>
                        <th>FECHA INICIO</th>
                        <th>FECHA FIN</th>
                        <th>ESTADO</th>
                        <th>ACCIONES</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($datos as $r)
                        @php
                            $result=\App\Models\Vacacion::getVacacion($r['gestion'],$trabajador->id);
                        @endphp
                        <tr>
                            <td>{{$r['gestion']}}</td>
                            <td>{{$r['antiguedad']}}</td>
                            <td>{{$r['dias_vaca']}}</td>
                            <td></td>
                            <td></td>
                            <td>

                                @if(!$result && $r['gestion']!=date('Y'))
                                    <h6 class=" label label-danger">VACACION PERDIDA</h6>
                                    @endif
                            </td>
                            <td>
                                @if( $r['gestion']==date('Y'))


                                <a href="#myModal{{$r['gestion']}}"  data-toggle="modal" class="btn  btn-xs btn-success"><i class="fa fa-gear"></i> Generar</a>
                                <div id="myModal{{$r['gestion']}}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">REGISTRAR VACACION</h4>
                                            </div>
                                            <form action="{{url('admin/tipofuncionario/update')}}" method="post"  class="form-horizontal" id="form" >
                                                {!! csrf_field() !!}
                                                <input type="hidden"  name="id" >
                                                <div class="modal-body">

                                                    <div class="form-group">
                                                        <label for="" class="col-md-3">FECHA REGISTRO</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="fecha_regsitro"  value="{{date('d/m/Y')}}" id="fecha_registro" class="date-picker form-control">
                                                        </div>

                                                    </div>
                                                    <div class="form-group">

                                                        @php
                                                            $vacacion=\App\Models\Vacacion::getVacacion($r['gestion']-1,$trabajador->id);

                                                             if($vacacion){
                                                             $dias=$vacacion->dias_total;
                                                             }
                                                              else{
                                                               $dias=0;
                                                              }
                                                        @endphp

                                                        <label for="" class=" col-md-3">DIAS ACUMULADOS
                                                        </label>
                                                        <div class="col-md-3">

                                                            <input type="text" name="dias_acumulados"  value="{{$dias}}" readonly id="dias_acumulados" class="date-picker form-control">
                                                        </div>

                                                                   @if($dias>0)
                                                        <label for="" class=" col-md-6">USAR DIAS ACUMULADOS
                                                            <input type="checkbox" id="cb_acum">
                                                        </label>
                                                                       @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-md-3">DIAS ASIGNADOS</label>
                                                        <div class="col-md-3">
                                                            <input type="number" name="dias_asignados"  readonly value="{{$r['dias_vaca']}}" id="dias_asignados" class=" form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-md-3">NUM DIAS PERMISO</label>
                                                        <div class="col-md-3">
                                                            <input type="number" name="dias_permiso" value="0"  id="dias_permiso" class=" form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-md-3">NUM DIAS TOTAL</label>
                                                        <div class="col-md-3">
                                                            <input type="number" name="dias_total"  readonly id="dias_total" class=" form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-md-3">FECHA INICIO</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="fecha_inicio"  value="{{date('d/m/Y')}}"  readonly id="fecha_inicio" class="date-picker form-control">
                                                        </div>
                                                        <label for="" class="col-md-2">FECHA FIN</label>
                                                        <div class="col-md-3">
                                                            <input  type="text"  readonly name="fecha_fin"  readonly id="fecha_fin" class=" form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-md-3">FECHA REINICIO</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="fecha_reinicio" value="{{date('d/m/Y')}}"  readonly id="fecha_reinicio" class="date-picker form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="" class="col-md-3">OBSERVACIONES</label>
                                                        <div class="col-md-6">
                                                            <textarea name="observaciones" id="observaciones"  rows="3" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                    <button type="submit" class="btn btn-success" >Guardar</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                <p>
                <h3 class="text-warning">No existen Vacaciones disponibles</h3>
                </p>
            @endif

        </div>


    </div>
@stop

@section('myscript')
    <script>
  function calular_fecha_fin(){
      var total=   $('#dias_total').val();
      var fi= $('#fecha_inicio').val();
      var url='/admin/ajax/obtenerFechaFinVacacion';
      $.ajax({
          url:url,
          method:'GET',
          data:{fecha_ini:fi,dias:total},
          dataType:'json',
          success:function(response){
              if(response.status){
                  $('#fecha_fin').val(response.fecha_fin);
              }

          },
          error:function(){}

      });
  }

        $(document).ready(function(){

            ///**** calculo de dias
            var dias_asig=parseInt($('#dias_asignados').val());
            var dias_per=parseInt($('#dias_permiso').val());
            calular_fecha_fin();

            $('#dias_total').val(dias_asig-dias_per);
            $('#cb_acum').click(function(){
                var dias_asig=parseInt($('#dias_asignados').val());
                var dias_acum=parseInt($('#dias_acumulados').val());
                var dias_per=parseInt($('#dias_permiso').val());
                if($(this).is(':checked')){
                    $('#dias_total').val(dias_asig+dias_acum-dias_per);
                }
                else {
                    $('#dias_total').val(dias_asig-dias_per);
                }
                calular_fecha_fin();
            });

            //////////////////////////////

            $('#fecha_inicio').change(function(){
                calular_fecha_fin();

            });

        });
    </script>
    @stop
