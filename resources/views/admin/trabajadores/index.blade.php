@extends('layouts.admin')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Trabajadores</h3>
            <div class="row">


            <a   href="{{url('admin/trabajadores/create')}}" class=" pull-left btn  btn-success"><i class="fa fa-plus"></i> Registrar nuevo trabajador </a>


                <div style="margin-top: -40px" class=" navbar-right col-md-2">

            <form action="{{url('/admin/trabajadores/filtrar')}}" method="get" class="form" id="form-filtrar">
                    {!!csrf_field()  !!}
                <div class="form-group">
                    <div class="col-md-12">
                    <label for="" class="control-label">Filtrar</label>
                    <select name="filtro" id="filtro" class="form-control">
                        <option value="" >SELECCIONAR</option>
                        <option value="ACTIVO" @if(isset($estado) && $estado=='ACTIVO') selected @endif>ACTIVOS</option>
                        <option value="INACTIVO" @if(isset($estado) && $estado=='INACTIVO') selected @endif>INACTIVOS</option>
                        <option value="TODOS" @if(isset($estado) && $estado=='TODOS') selected @endif>TODOS</option>
                    </select>
                    </div>
                </div>
            </form>
            </div>
            </div>
        </div>
        <div class="panel-body">
            <table class="dataTable table" id="table">
                <thead>
                <tr class="titulos">

                    <th>Tipo Doc/Num Doc</th>
                    <th>Nombres</th>
                    <th>Sexo</th>
                    <th>Est.Civil</th>
                    <th>Fecha Nac.</th>
                    <th>Cargo</th>
                    <th>Ubicacion</th>
                    <th>Proyecto/Planta</th>
                    <th>Sueldo</th>
                    <th>Acciones</th>
                </tr>
                </thead>

                @if(count($trabajadores)>0)
                    @foreach($trabajadores as  $row)
                        <tr @if(isset($estado) && $estado=='TODOS')@if($row->estado_laboral=='INACTIVO') class="bg-danger" @endif @endif>

                            <td>{{$row->tipo_documento}}/{{$row->num_documento}}</td>
                            <td>{{$row->nombres}} {{$row->apellidos}}</td>
                            <td>{{$row->sexo}}</td>
                            <td>{{$row->estado_civil}}</td>
                            <td>{{$row->fecha_nacimiento}}</td>
                            <td>{{$row->cargo}}</td>
                            <td>

                                @if(isset($row->lugar->nombre))
                                    {{$row->lugar->nombre}}/
                                    @endif
                                    @if(isset($row->seccion->nombre))
                                        {{$row->seccion->nombre}}
                                    @endif
                                    </td>
                            <td></td>
                            <td>{{$row->sueldo}}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-dark btn-xs"><i class="fa fa-gears"></i></button>
                                    <button type="button" class="btn btn-dark btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    @if($row->estado_laboral=='ACTIVO')
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">EDITAR</a>
                                        </li>
                                        <li>
                                            <a href="#myModal{{$row->id}}" data-toggle="modal">CAMBIAR INATIVO</a>
                                        </li>

                                        <li><a href="{{url('admin/vervacaciones')}}/{{$row->id}}">VER VACACIONES</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                        @else
                                        <ul class="dropdown-menu" role="menu">

                                            <li>
                                                <a href="#myModal{{$row->id}}" data-toggle="modal">CAMBIAR ACTIVO</a>
                                            </li>


                                            {{--<li><a href="#">Separated link</a></li>--}}
                                        </ul>
                                        @endif


                                </div>
                                <div id="myModal{{$row->id}}" class="modal fade" role="dialog">
                                    <div class="modal-dialog ">

                                        <!-- Modal content-->
                                        <div class="modal-content ">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">CAMBIAR A ESTADO @if($row->estado_laboral=='ACTIVO')  INACTIVO @else ACTIVO  @endif</h4>
                                            </div>
                                            <form action="{{url('admin/trabajadores/cambiarestado')}}" method="post"  class="form-horizontal" id="form" >
                                                {!! csrf_field() !!}
                                                <input type="hidden"  name="id" value="{{$row->id}}">
                                                <input type="hidden"  name="estado" value="{{$row->estado_laboral}}">
                                                <div class="modal-body">

                                                    <div class="form-group">
                                                        <label for="" class="col-md-2">NOMBRES</label>
                                                        <div class="col-md-8">
                                                            <input type="text" value="{{$row->nombre_completo}}" readonly name="nombre" id="nombre" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-md-2">FECHA</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="fecha"  value="{{date('d/m/Y')}}" id="fecha" class="date-picker form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                    <button type="submit" class="btn btn-success" >Guardar</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>

                                {{--<a href="" class="btn btn-warning btm-xs"><i  class="fa fa-pencil"></i></a>--}}


                            </td>
                        </tr>

                    @endforeach
                    @endif

            </table>
        </div>


    </div>
    @stop
@section('myscript')
    <script>
        $(document).ready(function(){

          $('.dataTable').dataTable({});

            $('#filtro').change(function () {

               if($(this).val()!=''){
                   $('#form-filtrar').submit();
               }
            });
        });
    </script>
    @stop