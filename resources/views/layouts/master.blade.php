<!DOCTYPE html>
<html lang="en">

<head>

    @yield('head')
    <title>@yield('title')</title>

   @yield('styles')

</head>


<body class="nav-md">
@yield('body')
@yield('scripts')
</body>

</html>
