<!-- Bootstrap core CSS -->

<link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet" >



<link href="{{ asset('fonts/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/animate.min.css')}}" rel="stylesheet">

<link href="{{asset('css/pnotify/pnotify.css')}}" rel="stylesheet">
<link href="{{asset('css/pnotify/pnotify.buttons.css')}}" rel="stylesheet">
<link href="{{asset('css/pnotify/pnotify.nonblock.css')}}" rel="stylesheet">

<!-- Custom styling plus plugins -->
<link href="{{ asset('/css/custom.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/maps/jquery-jvectormap-2.0.3.css')}}" />
<link href="{{ asset('css/icheck/flat/green.css')}}" rel="stylesheet"/>
<link href="{{ asset('css/floatexamples.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('js/datepicker/css/bootstrap-datepicker3.css')}}" rel="stylesheet" >

@yield('styles')