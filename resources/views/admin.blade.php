@extends('layouts.master')

@section('head')
    @include('includes.header')
    @endsection
@section('title')
admin
@endsection
@section('styles')
    @include('includes.style')
    @endsection

@section('body')
    <div class="container body">
        <div class="main_container">
            @include('includes.sidebar')


            @include('includes.head_top')
                    <!-- page content -->
            <div class="right_col" role="main">

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="dashboard_graph">

                            <div class="row x_title">
                                <div class="col-md-6">
                                    <h3>Empty <small>no content</small></h3>
                                </div>
                                <div class="col-md-6">

                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                ...
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
                <br />
            </div>
            <!-- /page content -->
            @include('includes.footer')
        </div>
    </div>
    @include('includes.footer_botton')

@endsection

@section('scripts')@include('includes.script')@endsection
