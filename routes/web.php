<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('admin');
});
Route::group(['prefix'=>'admin'],function (){




    //RUTAS AJAX

    Route::get('/ajax/obtenerFechaFinVacacion','VacacionController@obtenerFechaFinVacacion');


    //RUTAS TRABAJADORES

    Route::get('/trabajadores','TrabajadorController@index');
    Route::get('/trabajadores/create','TrabajadorController@create');
    Route::get('/trabajadores/create','TrabajadorController@create');
    Route::get('/trabajadores/filtrar','TrabajadorController@filtrar');
    Route::post('/trabajadores/cambiarestado','TrabajadorController@cambiarEstado');
    Route::post('/trabajadores/store','TrabajadorController@store');


    // vacaciones
    Route::get('/vervacaciones/{trabajador}','TrabajadorController@verVacaciones');


    //secciones
    Route::get('/secciones','SeccionController@index');
    Route::post('/secciones/store','SeccionController@store');
    Route::post('/secciones/update','SeccionController@update');
    Route::get('/secciones/destroy/{seccion}','SeccionController@destroy');

    //lugares
    Route::get('/lugares','LugarController@index');
    Route::post('/lugares/store','LugarController@store');
    Route::post('/lugares/update','LugarController@update');
    Route::get('/lugares/destroy/{lugar}','LugarController@destroy');

    //tipofuncionario
    Route::get('/tipofuncionario','TipoFuncionarioController@index');
    Route::post('/tipofuncionario/store','TipoFuncionarioController@store');
    Route::post('/tipofuncionario/update','TipoFuncionarioController@update');
    Route::get('/tipofuncionario/destroy/{tipo}','TipoFuncionarioController@destroy');

    //estado labarol
    Route::get('/estadolaboral','SeccionController@index');




});