<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Vacacion extends Model
{
    protected $table='vacaciones';
    protected $fillable=[
        'fecha_ini',
        'fecha_fin',
        'fecha_rein',
        'fecha_reg',
        'dias_vacacion',
        'dias_permiso',
        'observacion',
        'estado',
        'dias_total',
        'gestion',
        'trabajador_id',
        'codigo',
        'dias_acumulado',

    ];
    protected $date=[
        'fecha_ini',
        'fecha_fin',
        'fecha_rein',
        'fecha_reg',
    ];
    public   static  function getVacacion($gestion,$trabajador_id){
        $result=Vacacion::where('gestion',$gestion)
                        ->where('estado','ASIGNADO')
                        ->where('trabajador_id',$trabajador_id)
                        ->first();
        if(count($result)>0){
            return $result;
        }
        return false;
    }
    public  function setfechaIniAttribute($fecha){
        $this->attributes['fecha_ini']=Carbon::createFromFormat('d/m/Y',$fecha);

    }
    public  function setfechaFinAttribute($fecha){
        $this->attributes['fecha_fin']=Carbon::createFromFormat('d/m/Y',$fecha);

    }
    public  function setfechaReinAttribute($fecha){
        $this->attributes['fecha_rein']=Carbon::createFromFormat('d/m/Y',$fecha);

    }
    public  function setfechaRegAttribute($fecha){
        $this->attributes['fecha_reg']=Carbon::createFromFormat('d/m/Y',$fecha);

    }
}
