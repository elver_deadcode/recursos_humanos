<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstadoLaboralModel extends Model
{
    protected $table='estado_laboral';
    protected $fillable=[
        'codigo',
        'nombre'
    ];
     public $timestamps = false;


}
