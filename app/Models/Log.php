<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
      protected $table='log';
    protected  $fillable=[
        'tabla',
        'accion',
        'user',
        'descripcion',
        'id_tabla'
    ];
}
