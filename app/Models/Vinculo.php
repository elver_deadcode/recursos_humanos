<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vinculo extends Model
{
     protected  $table='vinculos';
    protected  $fillable=[
        'nombres',
        'parentesco',
        'fecha_nacimiento',
        'ci',
        'trabajadores_id'
    ];

    public  function trabajador(){
        return $this->belongsTo(Trabajador::class,'trabajadores_id');
    }
}
