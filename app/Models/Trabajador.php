<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
protected $table='trabajadores';
    protected  $fillable=[
        'tipo_documento',
    'numero_documento',
    'nombres',
    'apellidos',
    'sexo',
    'fecha_nacimiento',
    'estado_civil',
    'profesion',
    'direccion',
    'nro_social',
    'estado_laboral',
    'telefono_fijo',
    'telefono_celular',
    'telefono_emergencia',
    'telefono_coorporativo',
    'nro_licencia',
    'cat_licencia',
    'fecha_fenecimiento',
    'email_personal',
    'email_profesional',
    'num_cuenta',
    'cuenta_banco',
        'tipo_funcionario',
        'estado_laboral',
        'item',
        'cargo',
        'sueldo',
        'seccion_id',
        'lugar_id',
        'antiguedad_ext',
        'fecha_ingreso',
        'tipo_funcionario',
        'nacionalidad',
        'fecha_inactividad',


    ];



    protected $dates=[
        'fecha_ingreso',
        'fecha_fenecimiento',
        'fecha_nacimiento'

    ];


    public function  historial_estados(){
        return $this->hasMany(HistorialCambio::class,'trabajador_id');
    }
    public function vinculos(){
        return $this->hasMany(Vinculo::class,'trabajadores_id');


    }

    public function lugar(){
      return $this->belongsTo(LugarModel::class,'lugar_id');
    }

    public function  setfechaIngresoAttribute($fecha){
    $this->attributes['fecha_ingreso']=Carbon::createFromFormat('d/m/Y',$fecha);
}
    public function  setfechaFenecimientoAttribute($fecha){
        $this->attributes['fecha_fenecimiento']=Carbon::createFromFormat('d/m/Y',$fecha);
    }
    public function  setfechaNacimientoAttribute($fecha){
        $this->attributes['fecha_nacimiento']=Carbon::createFromFormat('d/m/Y',$fecha);
    }

    public function seccion(){
        return $this->belongsTo(LugarModel::class,'seccion_id');

    }

    public   function scopeAntiguedad($anio){

    $anios_ext=explode('/',$this->antiguedad_ext)[0];
    $antiguedad =Carbon::now()->year;
    $anios=Carbon::now()->year -$this->fecha_ingreso->year;
         return $anios_ext+$anios;

   }

   public  function antiguedad_por_anio($anio){
       $anios_ext=explode('/',$this->antiguedad_ext)[0];
       $anios=$anio-$this->fecha_ingreso->year;
       return $anios_ext+$anios;

    }

   public function getnombreCompletoAttribute(){
       return $this->nombres.' '.$this->apellidos;
   }
}
