<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $table='proyectos';
    protected $fillable=[
        'codigo',
        'descripcion'
    ];
    public $timestamps=false;
}
