<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    protected $table='contratos';
    protected  $fillable=[
        'idcontrato',
        'cod_contrato',
        'firma_contrato',
        'asesor',
        'observaciones'	,
        'fecha_ini'	,
        'fecha_fin'	,
        'tipo_contrato',
        'archivo'	,
        'trabajador_id'	,
        'proyecto_id',

    ];
    protected  $dates=['fecha_ini','fecha_fin'];
}
