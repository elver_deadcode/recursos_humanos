<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistorialCambio extends Model
{
protected  $table='historial_estado';
    protected  $fillable=[

        'estado',
        'created_at',
        'updated_at',
        'trabajador_id'
    ];
    public function  trabajador(){
        return $this->belongsTo(Trabajador::class,'trabajador_id');
    }
}
