<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LugarModel extends Model
{
    protected $table='lugares';
    protected  $fillable=[
        'codigo',
        'nombre',
    ];
    public $timestamps=false;
}
