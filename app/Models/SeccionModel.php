<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeccionModel extends Model
{
    protected  $table='secciones';
    protected  $fillable=[
        'tipo',
        'nombre'
    ];
    public $timestamps=false;
}
