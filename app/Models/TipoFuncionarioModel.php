<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoFuncionarioModel extends Model
{
    protected  $table='tipo_funcionario';
    protected $fillable=[
        'nombre',
        'codigo'
    ];

    public $timestamps=false;
}
