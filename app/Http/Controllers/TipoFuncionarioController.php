<?php

namespace App\Http\Controllers;

use App\Models\TipoFuncionarioModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class tipofuncionariocontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos =TipoFuncionarioModel::orderBy('nombre','ASC')->get();

        return view('admin.tipofuncionario.index',[
            'tipos'=>$tipos,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        Route::get('user', 'UserController@showProfile');




    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'codigo'=>'required',
            'nombre'=>'required'
        ];
        $messages=[
            'codigo.required'=>'El campo  codigo es obligatorio',
            'nombre.required'=>'El campo nombre es obligatorio',

        ];
        $this->validate($request,$rules,$messages);

        $tipo =  new TipoFuncionarioModel();
        $tipo->codigo=strtoupper($request->codigo);
        $tipo->nombre=strtoupper($request->nombre);
        $tipo->save();
        flash('El tipo de Funcionario  se registro <strong>Satisfactoriamente</strong>','info');

        return redirect('admin/tipofuncionario');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules=[
            'codigo'=>'required',
            'nombre'=>'required'
        ];
        $messages=[
            'codigo.required'=>'El campo  codigo es obligatorio',
            'nombre.required'=>'El campo nombre es obligatorio',

        ];
        $this->validate($request,$rules,$messages);

        $tipo =TipoFuncionarioModel::find($request->id);
        $tipo->codigo=strtoupper($request->codigo);
        $tipo->nombre=strtoupper($request->nombre);
        $tipo->save();
        flash('El tipo de Funcionario  se actualizo <strong>Satisfactoriamente</strong>','info');

        return redirect('admin/tipofuncionario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoFuncionarioModel $tipo)
    {
        try{
            $tipo->delete();
            flash('El tipo de Funcionario  se elimino <strong>Satisfactoriamente</strong>','info');


        }
        catch (\Exception $e){
            flash('Ocurrio un error <strong>Al Eliminar</strong>','warning');

        }
        return redirect('admin/tipofuncionario');

    }
}
