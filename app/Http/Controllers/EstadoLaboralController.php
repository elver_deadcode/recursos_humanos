<?php

namespace App\Http\Controllers;

use App\Models\EstadoLaboralModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class EstadoLaboralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados=EstadoLaboralModel::orderBy('nombre','ASC')->get();
        return view('admin.estado_laboral.index',[
            'estados'=>$estados
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'codigo'=>'required',
            'nombre'=>'required'
        ];
        $messages=[
            'codigo.required'=>'El campo  tipo es obligatorio',
            'nombre.required'=>'El campo nombre es obligatorio',

        ];
        $this->validate($request,$rules,$messages);

        $estado=new EstadoLaboralModel();
        $estado->codigo=strtoupper($request->codigo);
        $estado->nombres=$request->nombre;
        $estado->save();

         return   redirect('admin/estadolaboral');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
