<?php

namespace App\Http\Controllers;

use App\Models\SeccionModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class SeccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $secciones=SeccionModel::orderBy('nombre','ASC')->get();
        return view('admin.secciones.index',[

            'secciones'=>$secciones,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'tipo'=>'required',
            'nombre'=>'required'
        ];
        $messages=[
          'tipo.required'=>'El campo  tipo es obligatorio',
          'nombre.required'=>'El campo nombre es obligatorio',

        ];
        $this->validate($request,$rules,$messages);

        $seccion= new SeccionModel();
        $seccion->tipo= strtoupper($request->tipo);
        $seccion->nombre= strtoupper($request->nombre);
        $seccion->save();

         return redirect('admin/secciones');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules=[
            'tipo'=>'required',
            'nombre'=>'required'
        ];
        $messages=[
            'tipo.required'=>'El campo  tipo es obligatorio',
            'nombre.required'=>'El campo nombre es obligatorio',

        ];
        $this->validate($request,$rules,$messages);

        $seccion=SeccionModel::find($request->id);
        $seccion->tipo= strtoupper($request->tipo);
        $seccion->nombre= strtoupper($request->nombre);
        $seccion->save();

        return redirect('admin/secciones');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SeccionModel $seccion)
    {
        try{
            $seccion->delete();


        }
        catch (\Exception $e){
        }
        return redirect('admin/secciones');
    }
}
