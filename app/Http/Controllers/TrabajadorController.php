<?php

namespace App\Http\Controllers;

use App\Models\Contrato;
use App\Models\HistorialCambio;
use App\Models\Log;
use App\Models\LugarModel;
use App\Models\Proyecto;
use App\Models\SeccionModel;
use App\Models\TipoFuncionarioModel;
use App\Models\Trabajador;
use App\Models\Vinculo;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Jleon\LaravelPnotify\Notify;

class TrabajadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private  $fecha_control;
    public function index()
    {


        $trabajadores=Trabajador::where('estado_laboral','ACTIVO')
            ->orderBy('apellidos','ASC')->get();
        return view('admin.trabajadores.index',[
            'trabajadores'=>$trabajadores,
        ]);
    }
    public function __construct(){
        $this->fecha_control=Carbon::createFromFormat('Y-m-d','2022-01-01');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $lugares=LugarModel::orderBy('nombre','ASC')->get();
        $secciones=SeccionModel::orderBy('nombre','ASC')->get();
//        $tipos=TipoFuncionarioModel::orderBy('nombre','ASC')->get();
        $proyectos=Proyecto::orderBy('codigo','ASC')->get();
//        Notify::success('hola carajos', 'titulo');
        return view('admin.trabajadores.create',[
            'lugares'=>$lugares,
            'secciones'=>$secciones,
            'proyectos'=>$proyectos

        ]);
    }

    public function cambiarEstado(Request $request){

        $estado='';
        if($request->estado=='ACTIVO')
            $estado='INACTIVO';
                else
             $estado='ACTIVO';


        $t= Trabajador::find($request->id);
        $t->estado_laboral=$estado;
        $t->save();
        $hc=new HistorialCambio();
        $hc->estado=$estado;
        $hc->trabajador_id=$t->id;
        $hc->save();
        Log::create([
            'tabla'=>'trabajadores',
            'accion'=>'actualizar',
            'user'=>1,
            'descripcion'=>'cambio de estado de trabajador a '.$estado,
            'id_tabla'=>$t->id
        ]);

        Notify::success('El trabajador:'.$t->nombre_completo.' cambio su estado a '.$estado, 'Cambio de Estado');
           return redirect('admin/trabajadores');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        return dd($request->file());
//        return storage_path();
        $t=new Trabajador();
          $t->tipo_documento=$request->tipo_documento;
          $t->num_documento=$request->num_documento;
          $t->nombres=$request->nombres;
          $t->apellidos=$request->apellidos;
          $t->sexo=$request->sexo;
          $t->estado_civil=$request->estado_civil;
          $t->profesion=$request->profesion;
          $t->direccion=$request->direccion;
          $t->nro_social=$request->nro_social;
          $t->estado_laboral='ACTIVO';
          $t->telefono_fijo=$request->telefono_fijo;
          $t->telefono_celular=$request->telefono_celular;
          $t->telefono_emergencia=$request->telefono_emergencia;
          $t->telefono_coorporativo=$request->telefono_coorporativo;
          $t->nro_licencia=$request->nro_licencia;
          $t->cat_licencia=$request->cat_licencia;
          $t->fecha_fenecimiento=$request->fecha_fenecimiento;
          $t->email_personal=$request->email_personal;
          $t->email_profesional=$request->email_profesional;
          $t->num_cuenta=$request->num_cuenta;
          $t->item=$request->item;
          $t->cargo=$request->cargo;
          $t->sueldo=$request->sueldo;
          $t->lugar_id=$request->lugar_id;
          $t->seccion_id=$request->seccion_id;
          $t->antiguedad_ext=$request->anios_ant.'/'.$request->mes_ant.'/'.$request->dias_ant;
          $t->fecha_ingreso=$request->fecha_ingreso;
          $t->tipo_funcionario=$request->tipo_funcionario;

          $t->save();

        if(count($request->nombres_familiar)>0){

            for($i=0; $i<count($request->nombres_familiar);$i++){

                $v=new Vinculo();
                $v->nombres=$request->nombres_familiar[$i];
                $v->parentesco=$request->parentesco_familiar[$i];
                $v->fecha_nacimiento=$request->fechanac_familiar[$i];
                $v->ci=$request->carnet_familiar[$i];
                $v->trabajadores_id=$t->id;
                $v->save();
            }
        }
        if(count($request->cod_contrato)>0){

            foreach($request->cod_contrato as $key=>$v) {

                $c = new Contrato();
                $c->idcontrato = $request->idcontrato[$key];
                $c->cod_contrato = $request->cod_contrato[$key];
                $c->firma_contrato = $request->firma_contrato[$key];
                $c->asesor = $request->asesor[$key];
                $c->observaciones = $request->observaciones[$key];
                $c->fecha_ini = $request->fecha_ini[$key];
                $c->fecha_fin = $request->fecha_fin[$key];
                $c->tipo_contrato = $request->tipo_contrato[$key];

                if(isset($request->archivo[$key])){

//                        $destinationPath = public_path() . '/archivos'; // upload path
                        $filename = date("dmY-hi") . $request->archivo[$key]->getClientOriginalName();
                       Storage::disk('public')->put($filename,file_get_contents($request->archivo[$key]->getRealPath()));




                }

                $c->trabajador_id = $t->id;
                $c->proyecto_id = $request->proyecto[$key];
                $c->save();
            }



        }
        Notify::success('El registro del trabajador fue Exitoso', 'Registro Exitoso');


        return redirect('admin/trabajadores');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function filtrar(Request $request){
        $trabajadores=Trabajador::where('estado_laboral',$request->filtro)
                               ->orderBy('apellidos','ASC')->get();

        if($request->filtro=='TODOS'){
            $trabajadores=Trabajador::orderBy('apellidos','ASC')->get();
        }
        return view('admin.trabajadores.index',[
            'trabajadores'=>$trabajadores,
            'estado'=>$request->filtro
        ]);
    }


    public function verVacaciones(Trabajador $trabajador){



        $datos=[];
        $diff_anios=Carbon::now()->year-$trabajador->fecha_ingreso->year;

        $actual=Carbon::now();
       $diff_dias= $actual->diffInDays($trabajador->fecha_ingreso);

        


        $diff=361-$diff_dias;

        if($diff<=0){
            $anio_siguiente=$trabajador->fecha_ingreso->addYear(1)->year;

         for($i=$anio_siguiente;$i<$anio_siguiente+$diff_anios;$i++){

             $ant=$trabajador->antiguedad_por_anio($i);

           if($ant>1 && $ant<=5){
               $dv=15;
            }
             if($ant>5 && $ant<=10){
                $dv=20;
             }
             if($ant>10){
                $dv=30;
             }
             $datos[]=[
                 'gestion'=>$i,
                 'antiguedad'=>$ant,
                 'dias_vaca'=>$dv
             ];
        }
        }

        return  view('admin.trabajadores.vacaciones',[
            'trabajador'=>$trabajador,
            'datos'=>$datos

        ]);

}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
