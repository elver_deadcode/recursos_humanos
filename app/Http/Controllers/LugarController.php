<?php

namespace App\Http\Controllers;

use App\Models\LugarModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class LugarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $lugares=LugarModel::orderBy('nombre','ASC')->get();
        return view ('admin.lugares.index',[
            'lugares'=>$lugares,
        ]);
            }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'codigo'=>'required',
            'nombre'=>'required'
        ];
        $messages=[
            'codigo.required'=>'El campo  codigo es obligatorio',
            'nombre.required'=>'El campo nombre es obligatorio',

        ];
        $this->validate($request,$rules,$messages);

        $lugar =  new LugarModel();
        $lugar->codigo=strtoupper($request->codigo);
        $lugar->nombre=strtoupper($request->nombre);
         $lugar->save();
//        flash('El lugar se registro <strong>Satisfactoriamente</strong>','info');

        return redirect('admin/lugares');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)

    {
        $rules=[
            'codigo'=>'required',
            'nombre'=>'required'
        ];
        $messages=[
            'codigo.required'=>'El campo  codigo es obligatorio',
            'nombre.required'=>'El campo nombre es obligatorio',

        ];
        $this->validate($request,$rules,$messages);

        $lugar =  LugarModel::find($request->id);
        $lugar->codigo=strtoupper($request->codigo);
        $lugar->nombre=strtoupper($request->nombre);
        $lugar->save();
//        flash('El lugar se actualizo <strong>Satisfactoriamente</strong>','info');

        return redirect('admin/lugares');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LugarModel $lugar)
    {
        try {
            $lugar->delete();
//            flash('El lugar se elimino <strong>Satisfactoriamente</strong>','info');

        }
        catch (\Exception $e){
//            flash('Ocurrio un error <strong>Inesperado</strong>','warning');


        }
        return redirect('admin/lugares');
    }
    }
